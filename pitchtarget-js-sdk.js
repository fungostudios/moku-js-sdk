(function (global) {
  var PT = {
    init: init,
    assert: assert,
    api: api,
    get: get,
    post: post,
    testBodyParams: testBodyParams,

    signIn: signIn,
    signUp: signUp,
    signOut: signOut,

    baseApiUrl: "http://0.0.0.0:3000",
    headers: {
      'Accept': 'application/json'
    }
  };

  global.PT = PT;


/* ----------------- Core public methods --------------------- */

  // required: options is a js object, which must contains at least client_id
  function init(options) { 
    assert(options.client_id ,'client_id is not present in param object');

    localStorage.setItem("pt_client_id", options.client_id);
  }

  function assert(test, desc){
    if (!test) {
      throw "Assertion Failed: " + desc;
    }
  }

  // required: method, url
  // optional: callback (function that must have the following signature callback(error,response) )
  //           body (the data send with the request)
  //           headers (optional headers)
  //
  // return: ad_accounts array
  function api(method, url, body, headers, callback){
    assert((method && url), 'method and url parameters cannot be null');

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(e) {
      if (xhr.readyState === 4){ // if state == DONE
        if (xhr.status == 200){ // status OK
          //do something on success
          if ( presenceOfCallback(callback) ) return callback(null, xhr.responseText);

        } else { // return error from server
          if (xhr.status == 401) {
            localStorage.removeItem('pt_access_token');
            localStorage.removeItem('pt_user_id');
          }
          //do something on error
          if ( presenceOfCallback(callback) ) return callback(xhr.responseText, null);
        }
      }
    };

    xhrSettings(xhr, method, url, headers, body);
  }

  // this is a shortcut for api('GET', ....)
  function get(url, headers, callback) {

    return api('GET', url, null, headers, callback);
  }

  // this is a shortcut for api('POST', ....)
  function post(url, body, headers, callback) {

    return api('POST', url, body, headers, callback);
  }

  //required: --
  // optional: callback (function that must have the following signature callback(error,response) )
  //
  // return a string with some info
  function signOut(callback) {
    var url = PT.baseApiUrl + '/sign-out';
    var headers = JSON.parse(JSON.stringify(PT.headers)); // elegant way to clone an object
    headers.Authorization = 'OAuth ' + localStorage.getItem('pt_access_token');

    post(url, null, headers, callback);
    localStorage.removeItem('pt_access_token');
    localStorage.removeItem('pt_user_id');
  }

  // required: username, password
  // optional: callback (function that must have the following signature callback(error,response) )
  //
  // return: user obj and access_token
  function signIn(username, password, callback) {
    assert((username && password), 'password and username must be specified');

    var url = PT.baseApiUrl + '/oauth/authorize';
    var grant_type = "password";
    auth(url, username, password, grant_type, callback);
  }

  // required: username, password
  // optional: callback (function that must have the following signature callback(error,response) )
  //
  // return: access_token
  function signUp(username, password, callback){
    assert((username && password), 'password and username must be specified');

    var url = PT.baseApiUrl + '/sign-up';
    auth(url, username, password, null, callback);
  }

/* ----------------- Aux private methods --------------------- */

  // aux method for performing common authenticated get requests
  function getUrl(url) {
    var headers = JSON.parse(JSON.stringify(PT.headers)); // elegant way to clone an object
    headers.Authorization = 'OAuth ' + localStorage.getItem('pt_access_token');
    get(url, headers, callback); 
  }

  // used inside signIn and signUp
  function auth(url, username, password, grant_type, callback) {
    var client_id = localStorage.getItem('pt_client_id') || '';
    var body = {
      username: username,
      password: password,
      grant_type: grant_type, //added grant_type for signIn api calls
      client_id: client_id
    };
    var headers = JSON.parse(JSON.stringify(PT.headers)); // elegant way to clone an object
    headers["Content-Type"] = "application/json";

    assert((client_id !== ''), 'client_id is not present');
    
    if (!grant_type) {
      delete(body.grant_type);
      body = JSON.stringify(body);
    } else body = JSON.stringify(body);

    var authCall = function(error, response) {
      var res = JSON.parse(response);
      if (!error) {
        localStorage.setItem('pt_access_token', res.access_token);
        if (res.user) localStorage.setItem('pt_user_id', res.user.id);
        if (presenceOfCallback(callback)) return callback(null, res); 
      } else if (presenceOfCallback(callback)) return callback(res, null);
    };

    post(url, body, headers, authCall);
  }

  // return an object with stringified keys that contain array or object
  function testBodyParams(params) {
    var res = {};
    for (var key in params) {
      if (typeof params[key] == 'number' || typeof params[key] == 'string' || params[key] == "creative_images") res[key] = params[key]; 
      else res[key] = JSON.stringify(params[key]);
    }
    return res;
  }

  // params: xhr should be an xhr object, method a string like 'POST', 'GET', url, a valid url, body
  // should be a json object with data that should be passed to server
  // headers must be an object eg {'Authorization': 'OAuth kkkkkkkk', 'Accept': 'application/json', ...}
  //
  // the xhr object must be OPEN ( xhr.open(method, url, true) ) before setting headers
  function xhrSettings(xhr, method, url, headers, body){
    xhr.open(method, url, true);

    for(var header in headers) {
      xhr.setRequestHeader(header, headers[header]);
    }
    (body) ? xhr.send(body) : xhr.send();
  }

  //checks if callback params is a function
  function presenceOfCallback(callback) {

    return Function.prototype.isPrototypeOf(callback);
  }

  // test callback function
  function callback(error, response) {

    (!error) ? console.log(response) : console.log(error);
  }

  function serialize(obj, prefix) {
    var str = [];
    for(var p in obj) {
      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
      str.push(typeof v == "object" ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
  }
  // function checkSupportXhr2(){
  //   if('withCredentials' in new XMLHttpRequest() ) {
  //     return new XMLHttpRequest();
  //   }
  //   else {
  //     return something compatible with IE8
  //   }
  // }
})(window);